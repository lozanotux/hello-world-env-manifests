# hello-world-env manifests

This repository have the required manifests to deploy `hello-world-env` app in a kubernetes/openshift cluster. These manifests are packaged with **Kustomize**.

## How to validate the manifests?

To validate the manifests of each overlay, you can execute the next command:
```bash
$ kustomize build ./openshift/overlays/dev
```

> **NOTE:** for more information about `kustomize`, please go to the next article: [kubernetes.io/references/kustomize](https://kubectl.docs.kubernetes.io/references/kustomize/)